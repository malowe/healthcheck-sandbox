/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.health.sandbox;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Startup;
import org.eclipsefoundation.health.sandbox.CheckResource.CheckState;


/**
 * Startup check only seems to work in isolation, might be a rule I don't know.
 * 
 * @author martin
 *
 */
@Startup
@ApplicationScoped
public final class StartupCheck implements HealthCheck {
    @Inject
    CheckState state;

    @Override
    public HealthCheckResponse call() {
        return state.isHasStarted() ? HealthCheckResponse.up("Startup") : HealthCheckResponse.down("Startup");
    }
}
