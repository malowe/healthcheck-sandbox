package org.eclipsefoundation.health.sandbox;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/checks")
public class CheckResource {

    @Inject
    CheckState state;

    @GET
    @Path("readiness")
    @Produces(MediaType.TEXT_PLAIN)
    public Response readiness() {
        return Response.ok(state.isReady()).build();
    }

    @GET
    @Path("readiness/{state}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response readiness(@PathParam("state") boolean newState) {
        state.setReady(newState);
        return Response.ok(state.isReady()).build();
    }

    @GET
    @Path("liveness")
    @Produces(MediaType.TEXT_PLAIN)
    public Response liveness() {
        return Response.ok(state.isLive()).build();
    }

    @GET
    @Path("liveness/{state}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response liveness(@PathParam("state") boolean newState) {
        state.setLive(newState);
        return Response.ok(state.isLive()).build();
    }

    @GET
    @Path("startup")
    @Produces(MediaType.TEXT_PLAIN)
    public Response startup() {
        return Response.ok(state.isLive()).build();
    }

    @GET
    @Path("startup/{state}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response startup(@PathParam("state") boolean newState) {
        state.setHasStarted(newState);
        return Response.ok(state.isHasStarted()).build();
    }

    @Singleton
    public static final class CheckState {
        private static final Logger LOGGER = LoggerFactory.getLogger(CheckState.class);
        @ConfigProperty(name = "eclipse.initial.started", defaultValue = "true")
        Instance<Boolean> initialStarted;

        private boolean isReady;
        private boolean isLive;
        private boolean hasStarted;

        @PostConstruct
        public void init() {
            this.hasStarted = initialStarted.get();
            LOGGER.info("Initialized with {}",initialStarted.get());
        }

        /**
         * @return the isReady
         */
        public boolean isReady() {
            return isReady;
        }

        /**
         * @param isReady the isReady to set
         */
        public void setReady(boolean isReady) {
            this.isReady = isReady;
        }

        /**
         * @return the isLive
         */
        public boolean isLive() {
            return isLive;
        }

        /**
         * @param isLive the isLive to set
         */
        public void setLive(boolean isLive) {
            this.isLive = isLive;
        }

        /**
         * @return the hasStarted
         */
        public boolean isHasStarted() {
            return hasStarted;
        }

        /**
         * @param hasStarted the hasStarted to set
         */
        public void setHasStarted(boolean hasStarted) {
            this.hasStarted = hasStarted;
        }
    }
}