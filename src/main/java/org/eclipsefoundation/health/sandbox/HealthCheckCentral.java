/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.health.sandbox;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;
import org.eclipsefoundation.health.sandbox.CheckResource.CheckState;

/**
 * @author martin
 *
 */
public class HealthCheckCentral {

    @Readiness
    @ApplicationScoped
    public static final class ReadinessCheck implements HealthCheck {
        @Inject
        CheckState state;

        @Override
        public HealthCheckResponse call() {
            return state.isReady() ? HealthCheckResponse.up("Readiness") : HealthCheckResponse.down("Readiness");
        }
    }

    @Liveness
    @ApplicationScoped
    public static final class LivenessCheck implements HealthCheck {
        @Inject
        CheckState state;

        @Override
        public HealthCheckResponse call() {
            return state.isLive() ? HealthCheckResponse.up("Liveness") : HealthCheckResponse.down("Liveness");
        }
    }
}
